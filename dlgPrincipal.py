

import gi
gi.require_version("Gtk", "3.0")
from gi.repository import Gtk


class dlgPrincipal():
	
	def __init__(self):
		self.builder = Gtk.Builder()
		self.builder.add_from_file("/home/v/Descargas/ejemplo1/ui/ejemplo2.ui")
			
		self.dialogo = self.builder.get_object("dlgPrincipal")
		self.dialogo.resize(300, 200)
		self.dialogo.set_title("ventana de dialogo")
		self.dialogo.set_modal(True)
		
		boton_aceptar = self.dialogo.add_button(Gtk.STOCK_OK, Gtk.ResponseType.OK)
		
		boton_aceptar.set_always_show_image(True)
		boton_aceptar.connect("clicked", self.boton_aceptar_clicked)
		
		boton_cancelar = self.builder.get_object("btnCancelar")
		boton_cancelar.connect("clicked", self.boton_cancelar_clicked)
		
		self.nombre = self.builder.get_object("nombre")
		self.ciudad = self.builder.get_object("ciudad")
		
		self.dialogo.show_all()
		
	def boton_aceptar_clicked(self, btn=None):
		pass
	
	def boton_cancelar_clicked(self, btn=None):
		self.dialogo.destroy()
			