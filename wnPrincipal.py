

import gi
gi.require_version("Gtk", "3.0")
from gi.repository import Gtk
from dlgPrincipal import dlgPrincipal

class wnPrincipal():

	def __init__(self):
	
		self.valor_combobox = ""
		self.valor_combobox_text = ""
		
		self.builder = Gtk.Builder()
		self.builder.add_from_file("/home/v/Descargas/ejemplo1/ui/ejemplo2.ui")
		self.window = self.builder.get_object("wnPrincipal")
		self.window.connect("destroy", Gtk.main_quit)
		self.window.resize(600, 400)

		self.boton_abrir_dialogo = self.builder.get_object("botonAbrir")
		self.boton_abrir_dialogo.connect("clicked", self.abre_dialogo)

		self.label = self.builder.get_object("labelPrincipal")
		
		# combobox
		self.combobox = self.builder.get_object("combobox")
		render_text = Gtk.CellRendererText()
		self.combobox.pack_start(render_text, True)
		self.combobox.add_attribute(render_text, "text",0)
		self.modelo = Gtk.ListStore(str)
		
		
		self.combobox.connect("changed", self.combobox_changed)
		
		lista = ["Rodrigo", "Pepito"]
		for index in lista:
			self.modelo.append([index])
		
		self.combobox.set_model(self.modelo)
		
		
		#combotext
		self.comboboxtext = self.builder.get_object("comboboxtext")
		self.comboboxtext.connect("changed", self.comboboxtext_changed)
		self.comboboxtext.set_entry_text_column(0)
		for index in lista:
			self.comboboxtext.append_text(index)
		
		self.window.show_all()
		
	def abre_dialogo(self, btn=None):
		dlg = dlgPrincipal()
		response = dlg.dialogo.run()
		
		if response == Gtk.ResponseType.OK:
			print("Presiono el boton OK")
			nombre = dlg.nombre.get_text()
			ciudad = dlg.ciudad.get_text()
			
			self.modelo.append([nombre])
			self.comboboxtext.append_text(ciudad)
			
			texto = "".join([nombre, " ", ciudad])
			self.label.set_text(texto)
			dlg.dialogo.destroy()
		elif response == Gtk.ResponseType.CANCEL:
			print("Presiono el boton Cancelar")
		print(type(dlg.nombre))

	def combobox_changed(self, cmb=None):
		it = self.combobox.get_active_iter()
		modelo = self.combobox.get_model()
		self.valor_combobox = modelo[it][0]
		texto = "".join([self.valor_combobox_text, self.valor_combobox])
		self.label.set_text(texto)
	
	def comboboxtext_changed(self, cmb=None):
		self.valor_combobox_text = self.comboboxtext.get_active_text()
		texto = "".join([self.valor_combobox_text, self.valor_combobox])
		self.label.set_text(texto)
		
	
if __name__ == "__main__":
 	PRINCIPAL = wnPrincipal()
 	Gtk.main()

		
		
 