

import gi
gi.require_version("Gtk", "3.0")
from gi.repository import Gtk


class principalWindow():

    def __init__(self):

        builder = Gtk.builder()
        builder.add_from_file("./ui/ejemplo.ui")

        self.window = builder.get_object("window")
        self.window.connect("destroy", Gtk.main_quit)
        self.window.set_title("ESta es una ventana de ejemplo")
        self.window.resize("800, 600")
        print(self.window.get_title())

        self.label_principal = builder.get_object("label_principal")
        self.label_principal.set_text("Este es un ejemplo")

        self.boton = builder.get_object("boton_ejemplo")
        self.boton.connect("clicked", self.boton_click)
        self.boton.set_label("Click")

        self.reset = builder.get_object("boton_reset")
        self.reset.connect("clicked", self.reset)
        self.reset.set_label("Reset")

        self.entry = builder.get_object("entry_ejemplo")

        self.window.show_all()


    def reset_click(self, btn=None):
        self.entry.set_text("")
        self.label_principal.set_text("")


    def boton_click(self, btn=None):
        texto = self.entry.get_text()
        self.label_principal.set_text(texto)


if __name__ == "__main__":
    WINDOW = principalWindow()
    Gtk.main()
