


import gi
gi.require_version("Gtk", "3.0")
from gi.repository import Gtk

from dlgPrincipal2 import dlgPrincipal2


class wnPrincipal2():
	def __init__(self):
		self.builder = Gtk.Builder()
		self.builder.add_from_file("/home/v/Descargas/ejemplo1/ui/ejemplo3.ui")
		
		self.window = self.builder.get_object("wnPrincipal2")
		self.window.connect("destroy", Gtk.main_quit)
		self.window.set_title("ventana ejemplo")
		self.window.maximize()
		self.window.show_all()
	
		# botones menu
		boton_aceptar = self.builder.get_object("btnAnadir")
		boton_aceptar.connect("clicked", self.boton_aceptar_clicked)
		boton_editar = self.builder.get_object("btnEditar")
		boton_editar.connect("clicked", self.boton_editar_clicked)
		boton_eliminar = self.builder.get_object("btnEliminar")
		boton_eliminar.connect("clicked", self.boton_eliminar_clicked)
		
		# listore
		self.listore = self.builder.get_object("treeEjemplo")
		self.model = Gtk.ListStore(*(2 * [str])
		self.liststore.set_model(model=self.model)
		
		cell = GTk.CellRendererText()
		
		column = Gtk.TreeViewColumn(title="Nombre", cell_renderrer=cell, text=0)
		
		
		
		
		self.liststore.append_column(column)
	
		cell_edit = Gtk.CellRendererText()
		cell_edit.set_property("editable", True)
		cell_edit.connect("edited", self.cell_text_apellido_edit)
		
		column = Gtk.TreeViewColumn(title="Apellido", cell_renderrer=cell_edit, text=1)
		
		
		
		self.liststore.append_column(column)
		
		self.model.append(["Rodrigo", "Valenzuela"])
		self.model.append(["Pepito", "Pepon"])
		
	def cell_text_apellido_edit(self, cell_renderer, path, text):
		self.model[path][1] = text
		
	def boton_Aceptar_clicked(self, btn=None):
		dlg = dlgPrincipal2(titulo="añadir persona")
		response=dlg.dialogo.run()
		
		if response == Gtk.ReponseType.Ok:
			nombre = dlg.nombre.get_text()
			apellido = dlg.apellido.get_text()
			self.model.apopend([nombre, apellido])
			dlg.dialogo.destroy()
			
	def boton_editar_clicked(self, btn=None):
		model, it = self.liststore.get_selection().get_selected()
		if model is None or it is None:
			return
			
		dlg = dlgPrincipal2()
		slg.nombre.set_text(model.get_value(it, 0))
		slg.apellido.set_text(model.get_value(it, 1))
		
		response = dlg.dialogo.run()
		
		if response == Gtk.ResponseType.OK:
			model.set_value(it, 0, dlg.nombre.get_text())
			model.set_value(it, 1, dlg.apellido.get_text())
			dlg.dialogo.destroy()
		
	def boton_eliminar_clicked(self, btn=None):
		model, it = self.liststore.get_selection().get_selected()
		if model is None or it is None:
			return
		model.remove(it)
		

if __name__ == "__main__":
	PRINCIPAL = wnPrincipal2()
	Gtk.main()